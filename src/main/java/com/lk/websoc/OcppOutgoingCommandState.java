package com.lk.websoc;

/**
 * Created by kuli1 on 05.06.2017.
 */
public interface OcppOutgoingCommandState {

    void commandRecived(MessageController mt);
    void confirmationSent(MessageController mt);

}
