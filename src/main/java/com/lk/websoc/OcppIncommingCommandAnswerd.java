package com.lk.websoc;

/**
 * Created by kuli1 on 05.06.2017.
 */
public class OcppIncommingCommandAnswerd implements OcppIncommingCommandState {
    @Override
    public void commandSent(MessageController mt) {
        mt.setIncomingMessageProcessing();
    }

    @Override
    public void confirmationRecived(MessageController mt) {
        System.out.println("We should not get configrmation ! ");
    }
}
