package com.lk.websoc.model;

import java.util.Date;

/**
 * Created by kuli1 on 06.05.2017.
 */
public class BootNotificationResponse implements OcppMessage {

    private BootNotificationStatus status;
    private Date currentTime;
    private int interval;

    public BootNotificationStatus getStatus() {
        return status;
    }

    public void setStatus(BootNotificationStatus status) {
        this.status = status;
    }

    public Date getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Date currentTime) {
        this.currentTime = currentTime;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
