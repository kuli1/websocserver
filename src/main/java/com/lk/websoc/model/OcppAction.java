package com.lk.websoc.model;

/**
 * Created by kuli1 on 06.05.2017.
 */
public enum OcppAction {

    BootNotification,StatusNotification,Reset;
}
