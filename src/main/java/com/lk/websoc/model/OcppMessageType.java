package com.lk.websoc.model;

/**
 * Created by kuli1 on 06.05.2017.
 */
public enum OcppMessageType {
    OUTBOUND(2),INBOUND(3),ERROR(4);

    int code;
    private OcppMessageType(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }

}
