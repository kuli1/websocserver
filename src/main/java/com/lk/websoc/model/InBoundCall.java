package com.lk.websoc.model;

/**
 * Created by kuli1 on 06.05.2017.
 */
public class InBoundCall extends Call {
    public InBoundCall() {
        super(OcppMessageType.INBOUND);
    }
}
