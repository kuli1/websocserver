package com.lk.websoc.model;

import java.io.Serializable;

/**
 * Created by kuli1 on 16.04.2017.
 */
public class BootNotification implements Serializable {

    private String stationName;
    private String firmwareVersion;
    private String serialNumber;

    public BootNotification(String stationName, String firmwareVersion, String serialNumber) {
        this.stationName = stationName;
        this.firmwareVersion = firmwareVersion;
        this.serialNumber = serialNumber;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getStationName() {
        return stationName;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String toString() {
        return "BootNotification{" +
                "stationName='" + stationName + '\'' +
                ", firmwareVersion='" + firmwareVersion + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }
}
