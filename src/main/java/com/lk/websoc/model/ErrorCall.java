package com.lk.websoc.model;

/**
 * Created by kuli1 on 06.05.2017.
 */
public class ErrorCall extends Call {
    public ErrorCall() {
        super(OcppMessageType.ERROR);
    }
}
