package com.lk.websoc.model;

/**
 * Created by kuli1 on 06.05.2017.
 */
public class OutBoundCall extends Call {

    private OcppAction ocppAction;

    public OutBoundCall() {
        super(OcppMessageType.OUTBOUND);
    }

    public OcppAction getOcppAction() {
        return ocppAction;
    }

    public void setOcppAction(OcppAction ocppAction) {
        this.ocppAction = ocppAction;
    }
}
