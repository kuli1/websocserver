package com.lk.websoc.model;

import javax.naming.directory.InvalidAttributesException;

/**
 * Created by kuli1 on 06.05.2017.
 */
public abstract class Call {
    private OcppMessageType messageType;
    private long messageId;
    private OcppMessage palyload;

    public Call(OcppMessageType messageType) {
        this.messageType = messageType;
    }

    public static Call createCall(OcppMessageType messageType){
        switch (messageType){
            case INBOUND: return new InBoundCall();
            case ERROR: return new ErrorCall();
            case OUTBOUND: return new OutBoundCall();
            default: throw new RuntimeException("Bad Message Type ..");
        }
    }

    public OcppMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(OcppMessageType messageType) {
        this.messageType = messageType;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public OcppMessage getPalyload() {
        return palyload;
    }

    public void setPalyload(OcppMessage palyload) {
        this.palyload = palyload;
    }
}
