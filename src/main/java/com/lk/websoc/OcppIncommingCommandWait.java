package com.lk.websoc;

/**
 * Created by kuli1 on 05.06.2017.
 */
public class OcppIncommingCommandWait implements OcppIncommingCommandState {
    @Override
    public void commandSent(MessageController mt) {
        throw  new RuntimeException("System is already processing different message ! ");
    }

    @Override
    public void confirmationRecived(MessageController mt) {
        mt.setIncomingMessageAnswered();
    }
}
