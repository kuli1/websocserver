package com.lk.websoc;

/**
 * Created by kuli1 on 05.06.2017.
 */
public interface OcppIncommingCommandState {

    void commandSent(MessageController mt);
    void confirmationRecived(MessageController mt);

}
