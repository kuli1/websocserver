package com.lk.websoc.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created by kuli1 on 16.04.2017.
 */
public class MyMessageHandler extends TextWebSocketHandler {

    private final Logger LOG = LoggerFactory.getLogger(MyMessageHandler.class);

    @Autowired
    private ObjectMapper mapper;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
       // BootNotification bootNotificationMessage = mapper.readValue(message.getPayload(),BootNotification.class);
        LOG.info(String.format("Message from the client .. %s ",message));
        LOG.info("Station name is "+session.getAttributes().get("station_name"));
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        LOG.info("Connection Established with client...");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        LOG.info("Connecton closed with client..");
    }
}
