package com.lk.websoc;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by kuli1 on 05.06.2017.
 */
public class MessageController {

    Object outgoingMessageLock = new Object();
    String outgoingMessageId=null;
    Timer outMessageTimer = null;


    private static OcppIncommingCommandState incomingMessageProcessing = new OcppIncommingCommandWait();
    private static OcppIncommingCommandState incomingMessageAnswered = new OcppIncommingCommandAnswerd();


    private OcppIncommingCommandState incommingMessageState = incomingMessageAnswered;

    void setIncomingMessageAnswered(){
        incommingMessageState = incomingMessageAnswered;
    }

    void setIncomingMessageProcessing(){
        incommingMessageState = incomingMessageProcessing;
    }

    private Timer setTimout(final String myMessageId){
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
               try {
                   synchronized (outgoingMessageLock) {
                       if (myMessageId.equals(outgoingMessageId)) {
                           setIncomingMessageAnswered();
                           outMessageTimer.cancel();
                           System.out.println("Send Error Message To the server");
                       }
                   }
               }
               catch (Exception ex){
                   System.out.println("Exception from Timer Task ");
                   ex.printStackTrace();
               }
            }
        };

        //Timout 20 sec
        timer.schedule(timerTask,20000);
        return timer;

    }

    private void cancelTimer(){
        outMessageTimer.cancel();
    }

    //We can't get send more than 1 message at time !
    public void sendServerMessage(String message,String messageID){

        synchronized (outgoingMessageLock) {
            incommingMessageState.commandSent(this);
            System.out.println("Server sent message  : " + message);
            outgoingMessageId = messageID;
            outMessageTimer = setTimout(messageID);
        }
    }

    public void getMessageConfirmation(String message,String messageId){
        synchronized (outgoingMessageLock) {
            if(messageId.equals(outgoingMessageId)) {
                cancelTimer();
                System.out.println("We got confirmation for message : " + message);
                outgoingMessageId = null;
                incommingMessageState.confirmationRecived(this);
            }
            else {
                System.out.println("Message Confirmation dropped as it was timouted !");
            }
        }
    }

    //We can't process more than one message at time !
    public void getStationMessage(String message){

    }

}
