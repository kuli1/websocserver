package com.lk.websoc;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.*;

/**
 * Created by kuli1 on 05.06.2017.
 */
@SpringBootTest
public class MessageControllerTest {


    @Test
    public void should_not_allow_to_send_two_calls() throws InterruptedException {

        MessageController mt =  new MessageController();


        String message1 = "Message 1";
        String message2 = "Message 2";
        String message3 = "Message 3";


        new Thread(() -> mt.sendServerMessage(message1,"1234")).start();
        Thread.sleep(30000);
        new Thread(() -> mt.sendServerMessage(message2,"5678")).start();
        Thread.sleep(10000);
        new Thread(() -> mt.getMessageConfirmation(message1,"1234")).start();
        new Thread(() -> mt.getMessageConfirmation(message2,"5678")).start();
        new Thread(() -> mt.sendServerMessage(message3,"9999")).start();





    }

}